# rokot-test

Rokot - [Rocketmakers](http://www.rocketmakers.com/) TypeScript NodeJs Platform

## Introduction

A typescript testing library for nodejs applications

Created to bring together the suite of testing tools we use and make writing test scripts as simple as possible.

## Contributing

### Installation
After pulling the source
```
npm i
```

## Getting Started

### Installation
Install via `npm`
```
npm i rokot-test --save-dev
```

### Setup

Modify your `package.json` to invoke `mocha` within the test script (ensure you compile the typescript before running `mocha`):
* WebPack
```
{
  "scripts": {
    "test": "webpack --progress && mocha"
  }
}
```

* Typescript
```
{
  "scripts": {
    "test": "tsc -p source && mocha"
  }
}
```

Create a `test` folder within the root of your project and create a `mocha.opts` file containing the path to your compiled js test file(s):
```
dist/tests/*-test.js
```

## Example

Create a `<test>.ts` file:
```typescript
import {expect} from "rokot-test";

describe("Every Application should have a Test suite", () => {
  it("should cover all functional areas of your code base", () => {
    var testers = {};
    var developers = testers;

    expect(developers).to.be.eq(testers);
  });
})
```
The full set of imports are:
```
import {expect,sinon,supertest,chai} from "rokot-test";
```
Now, become a tester!!!

### Running

To execute your tests (via the test script you specified earlier within `package.json`):
```
npm test
```


## Consumed Libraries

### [mocha](https://mochajs.org/)
test framework engine (including teamcity reporter)
+ [teamcity reporter](https://github.com/travisjeffery/mocha-teamcity-reporter)

### [chai](http://chaijs.com/)
assertions library (with additional plugins)
+ promise (via [chai-as-promised](https://github.com/domenic/chai-as-promised))
+ array (via [chai-things](https://github.com/chaijs/chai-things))

### [sinon](http://sinonjs.org/)
Test spies, stubs and mocks

### [supertest](https://github.com/visionmedia/supertest)
HTTP assertions (for express route testing)
